export interface GistDataItemFile {
  filename: string;
  type: string;
  language: string;
  raw_url: string;
  size: number;
}
export interface GistDataItem {
  id: string;
  files: Record<string, GistDataItemFile>;
  public: boolean;
  created_at: string;
  updated_at: string;
  description: string;
  url: string;
  html_url: string;
  owner: {
    login: string;
    id: number;
    avatar_url: string;
    html_url: string;
  };
}
