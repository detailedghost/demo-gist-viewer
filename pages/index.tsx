import Head from "next/head";
import GistList from "../components/SearchList/GistList";
import GistSearch from "../components/GistSearch";
import { useState } from "react";
import useSWR from "swr";
import CustomApi from "../libs/custom-api.lib";
import { GistDataItem } from "../dto/models";
import { fetcher } from "../util/http.util";
import Alert from "../components/common/Alert";
import { has, union } from "lodash";
import { getLocalStorageIds } from "../hooks/useLocalStorageHook";

//? Got tired, and just grinded out
const getFavFullGist = () => getLocalStorageIds("gist")(undefined);
const getFavFileGist = () =>
  getLocalStorageIds("gistFiles")(item => item.id.split(":")[0]).reduce(
    (acc, cur) => (acc.indexOf(cur) > -1 ? acc : [...acc, cur]),
    []
  );

const grabUrl = (query?: string) => {
  if (!query) return CustomApi.gist.all();

  const id = query.substr(query.indexOf("id:") + 3, 32);
  if (query.indexOf("id:") > -1) return CustomApi.gist.byId(id);
  if (query.indexOf("username:") > -1) {
    return CustomApi.gist.byUsername(
      query.substr(query.indexOf("username:") + 9)
    );
  }
  if (query.indexOf("!fav-only") > -1) {
    return CustomApi.gist.favorites(union(getFavFullGist(), getFavFileGist()));
  }

  return CustomApi.gist.byUsername(query);
};

interface GistErrorResponse {
  message: string;
}
type GistResponse = GistDataItem[] | GistErrorResponse;

export default function Home() {
  const [searchTerm, setSearchTerm] = useState<string>("");
  const { data, error } = useSWR<GistResponse>(grabUrl(searchTerm), fetcher, {
    refreshInterval: 10 * 60 * 1000, //? Slow down # of auto-requests
    refreshWhenHidden: false,
    refreshWhenOffline: false,
  });
  return (
    <>
      <Head>
        <title>Github Gist Viewer {"\u{1F419}"} </title>
        <meta name="description" content="Demo application for Gist Viewer" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <section className="fixed top-0 lg:top-2 w-full px-6 bg-transparent z-20">
          <GistSearch className="mx-auto" onChange={setSearchTerm}></GistSearch>
        </section>
        <section className="mt-20 grid w-4/5 mx-auto">
          {(() => {
            if (error) {
              return (
                <Alert
                  type="danger"
                  message={error.message || "Ran into an error"}
                />
              );
            }
            if (has(data, "message")) {
              return (
                <Alert
                  type="danger"
                  message={(data as GistErrorResponse).message}
                />
              );
            }
            if (!data) {
              return <Alert type="info" message={"Loading..."} />;
            }
            return <GistList items={data as GistDataItem[]} />;
          })()}
        </section>
      </main>
    </>
  );
}
