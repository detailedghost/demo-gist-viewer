import { NextApiRequest, NextApiResponse } from "next";
import { isGet } from "../../util/next-api.util";

export default (req: NextApiRequest, res: NextApiResponse) => {
  if (isGet(req)) {
    res.status(200).json({ status: "good" });
    return;
  }

  res.status(403).end();
};
