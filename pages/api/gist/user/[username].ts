import { NextApiRequest, NextApiResponse } from "next";
import { isGet } from "../../../../util/next-api.util";
import { Gist } from "../../../../libs/github.lib";

export default async function (req: NextApiRequest, res: NextApiResponse) {
  const { username } = req.query;
  if (isGet(req) && username) {
    try {
      const list = await Gist.getByUserName(username as string);
      res.status(200).json(list);
    } catch (err) {
      res.status(500).send({
        message: "Issue with getting list: " + err,
      });
    }
  }

  return res.status(400).end();
}
