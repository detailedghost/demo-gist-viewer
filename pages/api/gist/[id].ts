import { NextApiRequest, NextApiResponse } from "next";
import { isGet } from "../../../util/next-api.util";
import { Gist } from "../../../libs/github.lib";

export default async function (req: NextApiRequest, res: NextApiResponse) {
  const { id } = req.query;
  if (isGet(req)) {
    try {
      const list = await Gist.getById(id as string);
      res.status(200).json(list);
      return;
    } catch (err) {
      res.status(500).send({
        message: "Issue with getting list: " + err,
      });
      return;
    }
  }

  res.status(400).end();
}
