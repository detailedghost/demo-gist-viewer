import { NextApiRequest, NextApiResponse } from "next";
import { Gist } from "../../../../libs/github.lib";
import { isGet } from "../../../../util/next-api.util";

export default async function (req: NextApiRequest, res: NextApiResponse) {
  const { ids } = req.query;
  if (isGet(req)) {
    const batch = (ids as string).split(",");
    try {
      const responses: { status: string; value?: unknown }[] =
        await Gist.getByIds(batch);
      return res
        .status(200)
        .send(responses.filter(r => !!r.value).map(r => r.value));
    } catch (error) {
      return res.status(500).send({
        message:
          "Server issue, probably do to a 'small' github request flood \u{1F923}",
      });
    }
  }
  return res.status(400).send({ message: "Bad request" });
}
