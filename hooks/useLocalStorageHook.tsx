import { useEffect } from "react";
import { useState } from "react";

type CacheSetTypes = "gist" | "gistFiles";
type CacheRecordItem = { id: string; value: unknown };

const hasLocalStorage =
  typeof window !== "undefined" && "localStorage" in window;

const getLocalData = key =>
  hasLocalStorage
    ? (JSON.parse(localStorage.getItem(key)) as CacheRecordItem[])
    : ([] as CacheRecordItem[]);

export const getLocalStorageIds =
  (setName: string) => (idSelector?: (item: CacheRecordItem) => string) =>
    hasLocalStorage && getLocalData(setName)
      ? getLocalData(setName).map(idSelector ? idSelector : item => item.id)
      : [];

export function useLocalStorage(setName: CacheSetTypes, initial = []) {
  const previous = hasLocalStorage ? localStorage.getItem(setName) : "";
  const [db, setDb] = useState<CacheRecordItem[]>(
    previous ? JSON.parse(previous) : initial
  );
  useEffect(() => {
    if (hasLocalStorage) localStorage.setItem(setName, JSON.stringify(db));
  }, [db]);

  const getLocal = () => getLocalData(setName);

  return [db, setDb, getLocal] as const;
}
