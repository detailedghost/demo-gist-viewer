import { createUrl } from "../util/http.util";

const createCustomApi = createUrl("api");

export default class CustomApi {
  static health = () => createCustomApi("health");
  static get gist() {
    return {
      all: () => createCustomApi("gist"),
      byUsername: (username: string) =>
        createCustomApi(`gist/user/${username}`),
      byId: (id: string) => createCustomApi(`gist/${id}`),
      favorites: (gistIds: string[] = []) =>
        createCustomApi(`gist/favorites?ids=${gistIds.join(",")}`),
    };
  }
}
