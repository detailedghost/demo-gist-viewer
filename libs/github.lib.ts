import { cache, createUrl, fetcher } from "../util/http.util";

const GITHUB_BASE = "https://api.github.com";

const createGithubUrl = (pathname: string): string =>
  createUrl(GITHUB_BASE)(pathname);

const createGithubGistsUrl = (pathname: string): string =>
  createGithubUrl(`gists/${pathname}`);

const createGithubUserUrl = (username: string, pathname: string): string =>
  createGithubUrl(`users/${username}/${pathname}`);

//? Wanted to keep separate for file spacing concern
const GistUrls = {
  publicList: () => createGithubGistsUrl("public"),
  byId: (id: string) => createGithubGistsUrl(`${id}`),
  byUserName: (username: string) => createGithubUserUrl(username, "gists"),
};

const cacheMap = {
  username: new Map<string, any>(),
  ids: new Map<string, any>(),
};

//* Facade pattern with modules
export class Gist {
  static URLS = GistUrls;
  static getList = () => fetcher(Gist.URLS.publicList());
  static getById = (id: string) =>
    cache(fetcher, cacheMap.ids)(Gist.URLS.byId(id));
  static getByIds = async (ids: string[]) =>
    await Promise.allSettled(ids.map(i => Gist.getById(i)));
  static getByUserName = (username: string) =>
    cache(fetcher, cacheMap.username)(Gist.URLS.byUserName(username));
}
