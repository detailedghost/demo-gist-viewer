import { faEye, faEyeSlash, faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Image from "next/image";
import { useState } from "react";
import { GistDataItem } from "../../dto/models";
import GistListItemFile from "./GistListItemFile";

//? Went for only stuff we care about
interface GistListItemProps {
  data: GistDataItem;
  isFav: boolean;
  setIsFav: (value) => void;
}
const GistListItem = ({ data, isFav, setIsFav }: GistListItemProps) => {
  const FILES_NUM = Object.keys(data.files).length;
  const { owner, id, files, description, url } = data;
  const [filesViewable, setFilesViewable] = useState<boolean>(FILES_NUM < 3);
  const onSetFav = e => setIsFav(!isFav);

  return (
    <article className="w-full h-full relative overflow-x-hidden px-4">
      <aside className="absolute top-1 left-2">
        <button
          className="flex outline-none focus:ring-transparent focus:outline-none outline-none"
          onClick={onSetFav}
        >
          <span
            className={`w-6 h-6 transition ${
              isFav ? "text-yellow-400" : "text-gray-400"
            }`}
          >
            <FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
          </span>
          <span className="ml-2">Favorite</span>
        </button>
      </aside>
      <header className="mt-8">
        <h3>
          <strong>ID:</strong>
          <a
            href={data.html_url}
            className="ml-1 text-blue-800 hover:text-blue-400 underline"
          >
            {id}
          </a>
        </h3>
      </header>
      <main className="mt-2">
        <section>
          <strong>Description</strong>
          <p className="pr-4">
            {description && description.length ? description : "None..."}
          </p>
        </section>
        <section className="mt-2">
          <div className="flex">
            <h4 className="font-bold">Files ({FILES_NUM}): </h4>
            <button
              className="ml-2 w-6 h-6 outline-none focus:outline-none"
              onClick={() => setFilesViewable(!filesViewable)}
              role="toggle"
            >
              <FontAwesomeIcon
                className="text-gray-700"
                icon={filesViewable ? faEye : faEyeSlash}
              />
            </button>
          </div>
        </section>
        <ul className={`mb-2 ${filesViewable ? "" : "hidden"}`}>
          {Object.keys(files).map((key, i) => (
            <li className="mb-1" key={`file-${id}-${i}`}>
              <GistListItemFile
                stateKey={`${id}:${files[key].filename}`}
                data={files[key]}
              ></GistListItemFile>
            </li>
          ))}
        </ul>
      </main>
      <footer className="w-full flex border-t-2 border-dashed border-gray-200 p-2 z-10 bg-white mt-2">
        <a
          href={owner.html_url}
          target="_blank"
          rel="noreferrer"
          className="rounded-full w-8 h-8 relative overflow-hidden border-4 border-blue-400 hover:border-light-blue-800"
        >
          <Image layout="fill" src={owner.avatar_url} alt="Avatar"></Image>
        </a>
        <div className="flex-auto flex items-center pl-2">
          <span>{owner.login}</span>
        </div>
      </footer>
    </article>
  );
};

export default GistListItem;
