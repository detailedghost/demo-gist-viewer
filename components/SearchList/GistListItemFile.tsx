import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { first } from "lodash";
import { useState } from "react";
import { GistDataItemFile } from "../../dto/models";
import { useLocalStorage } from "../../hooks/useLocalStorageHook";

interface GistListItemFileProps {
  stateKey: string;
  data: GistDataItemFile;
}

const GistListItemFile = ({ stateKey, data }: GistListItemFileProps) => {
  const [getFileFavs, setFileFavs, getLocal] = useLocalStorage("gistFiles");
  const foundFile = first(getFileFavs.filter(items => items.id === stateKey));
  const [fav, setFav] = useState(
    foundFile ? (foundFile.value as boolean) : false
  );

  const onClickAction = () => {
    setFav(!fav);
    const newItem = { id: stateKey, value: !fav };
    return !foundFile
      ? setFileFavs([...getLocal(), newItem])
      : setFileFavs(
          getLocal().map(item => (item.id === stateKey ? newItem : item))
        );
  };

  return (
    <div className="flex">
      <button
        className="block mr-1 w-6 h-6 outline-none focus:outline-none"
        onClick={onClickAction}
      >
        <FontAwesomeIcon
          className={`transition ${fav ? "text-yellow-400" : "text-gray-400"}`}
          icon={faStar}
        ></FontAwesomeIcon>
      </button>
      <a
        href={data.raw_url}
        target="_blank"
        rel="noreferrer"
        className="text-blue-800 hover:text-blue-400 underline"
      >
        {data.filename}
      </a>
    </div>
  );
};
export default GistListItemFile;
