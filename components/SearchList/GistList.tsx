import { FC } from "react";
import { GistDataItem } from "../../dto/models";
import { useLocalStorage } from "../../hooks/useLocalStorageHook";
import GistListItem from "./GistListItem";

interface GistListProps {
  items: GistDataItem[];
}

const GistList: FC<GistListProps> = ({ items }: GistListProps) => {
  const [favItems, setFavItems] = useLocalStorage("gist");

  const getFavItem = id => {
    if (!favItems) return false;
    const found = favItems.filter(i => i.id === id);
    return found && found.length > 0 && (found[0].value as boolean);
  };

  const onSetFav = (id, value) => {
    const found = favItems.filter(i => i.id === id).length > 0;
    setFavItems(
      !found
        ? [...favItems, { id, value }]
        : favItems.map(i => (i.id === id ? { id, value } : i))
    );
  };

  return (
    <ul className="flex flex-wrap border-1 border-red-400 w-full ">
      {items.map(item => {
        return (
          <li className="w-full lg:w-1/3 mx-auto mb-2" key={item.id}>
            <div className="lg:m-2 shadow-md ">
              <GistListItem
                data={item}
                isFav={getFavItem(item.id)}
                setIsFav={value => onSetFav(item.id, value)}
              ></GistListItem>
            </div>
          </li>
        );
      })}
    </ul>
  );
};

export default GistList;
