import { useState } from "react";

export type AlertType = "default" | "danger" | "info" | "success";

interface AlertProps {
  type: AlertType;
  title?: string;
  message?: string;
  dismissable?: boolean;
  children?: any;
}

const Alert = ({ title, type, message, children, dismissable }: AlertProps) => {
  const [isOpen, setIsOpen] = useState(true);
  const colorMap: Record<AlertType, string> = {
    default: "bg-gray-200",
    danger: "bg-red-400",
    info: "bg-blue-400",
    success: "bg-green-200",
  };
  const titleMap: Record<AlertType, string> = {
    default: "\u{1F44B} Alert",
    danger: "\u{2757} There was an issue",
    info: "\u{1F44B} Alert",
    success: "\u{1F389} Success",
  };
  return (
    <dialog
      open={isOpen}
      className={`absolute bottom-0 w-1/2 text-white border-2 border-red-200 shadow m-auto p-4 rounded-md ${colorMap[type]}`}
    >
      {!dismissable ? (
        <></>
      ) : (
        <a
          href="javascript:void(0)"
          className="absolute top-0 right-2 text-lg color-gray-400 hover:color:gray-800 font-bold"
          onClick={() => setIsOpen(false)}
        >
          X
        </a>
      )}
      <h1>{title ?? titleMap[type]}</h1>
      {message ? <p>{message}</p> : <></>}
      {children}
    </dialog>
  );
};

export default Alert;
