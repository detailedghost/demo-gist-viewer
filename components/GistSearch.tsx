import {
  faRedoAlt,
  faSearch,
  faStar,
  faStickyNote,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useCallback, useEffect, useState } from "react";

interface GistSearchProps {
  className?: string;
  onChange?: (query: string) => void;
}

const GistSearch = (props: GistSearchProps) => {
  const [search, setSearch] = useState<string>("");
  const [showNote, setShowNote] = useState<boolean>(false);

  const trigger = query => {
    props.onChange(query);
    if (search !== query) setSearch(query);
  };

  return (
    <section className={props.className}>
      <label className="sr-only">Search...</label>
      <div className="relative rounded-md flex rounded-sm shadow-xl ">
        <input
          type="search"
          name="email"
          id="email"
          className="focus:ring-indigo-200 focus:border-indigo-200 flex-auto sm:text-sm w-full p-4"
          placeholder="Search..."
          value={search}
          onChange={e => setSearch(e.target.value)}
          onKeyDown={e =>
            e.key.toLowerCase() === "enter" ? setSearch(search) : undefined
          }
        />
        <div className="flex text-black justify-end">
          <button
            onClick={_ => trigger(search)}
            className="w-6 mx-2 text-green-600 hover:text-green-800 outline-none focus:outline-none"
          >
            <FontAwesomeIcon icon={faSearch}></FontAwesomeIcon>
          </button>
          <button
            onClick={_ => trigger("!fav-only")}
            className="w-6 mx-2 text-yellow-600 hover:text-yellow-400 outline-none focus:outline-none"
          >
            <FontAwesomeIcon icon={faStar}></FontAwesomeIcon>
          </button>
          <button
            onClick={_ => trigger("")}
            className="w-6 mx-2 text-black hover:text-gray-600 outline-none focus:outline-none"
          >
            <FontAwesomeIcon icon={faRedoAlt}></FontAwesomeIcon>
          </button>
          <button
            onClick={_ => setShowNote(!showNote)}
            className="w-6 mx-2 text-yellow-200 hover:text-yellow-400 outline-none focus:outline-none"
          >
            <FontAwesomeIcon icon={faStickyNote}></FontAwesomeIcon>
          </button>
        </div>
      </div>
      <div
        className={`p-2 bg-yellow-200 border-2 border-gray-400 ${
          showNote ? "" : "hidden"
        }`}
      >
        <h4>{"\u{1F4D3}"} Note to users:</h4>
        <ul>
          <li>{"Search defaults to searching by username!"}</li>
          <li>
            {'To search by gist id, type "id:" + username to find gist by id'}
          </li>
          <li>
            {
              'To search by username, type "username:" + username to find gist by username'
            }
          </li>
          <li>
            {
              'clear to see all, type "username:" + username to find gist by username'
            }
          </li>
        </ul>
      </div>
    </section>
  );
};

export default GistSearch;
