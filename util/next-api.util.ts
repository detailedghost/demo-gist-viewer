import { NextApiRequest } from "next";

const isMethod = (shouldBe: string) => (req: NextApiRequest) =>
  req.method.toUpperCase() === shouldBe.toUpperCase();

export const isGet = isMethod("get");
export const isPost = isMethod("post");
export const isPut = isMethod("put");
export const isPatch = isMethod("patch");
export const isDelete = isMethod("delete");

export const httpUtil = {
  method: {
    isGet,
    isPost,
    isPatch,
    isDelete,
  },
};
