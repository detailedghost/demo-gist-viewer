//? This is needed to setup SWR. Docs here > https://swr.vercel.app/
//* Just going to use regular Fetch for both BE and FE
export const fetcher = (...args) =>
  fetch(args[0], args[1]).then(res => res.json());

export const cache = (
  fetcher: (args: any[]) => Promise<any>,
  ref?: Map<string, unknown>
) => {
  const localCache = ref ?? new Map<string, unknown>();
  return (...args) => {
    if (localCache.has(args[0]) && localCache.get(args[0])) {
      return Promise.resolve(localCache.get(args[0]));
    }
    return fetcher(args)
      .then(results => {
        localCache.set(args[0], results);
        return localCache.get(args[0]);
      })
      .catch(err => {
        localCache.set(args[0], null);
      });
  };
};

export const reduceJsonToQuery = (obj: Record<string, string | number>) =>
  !obj
    ? ""
    : Object.entries(obj)
        .reduce(
          (acc, [key, value]) => (!value ? acc : (acc += `&${key}=${value}`)),
          ""
        )
        .substr(1);

//? Kept them up here incase I need to use for multiple static classes
export const createUrl = (base: string) => (pathname: string) =>
  `${base}/${pathname}`;
