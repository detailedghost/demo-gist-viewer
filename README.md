# Github gist Viewer Demo

- author: Dante Burgos
- created: 5/16/2021

## introduction

This is a simple gist viewer application that users can log into to check public gists, star, and search.

### why next js?

Wanted to use a pretty solid framework that provided api endpoints for quick development.
It provides React JS out-of-the-box that should highlight some of the talents I had. I had a short timeframe,
and wanted to make the most out of it.

### how long did this take?

About 3.5 hours. Had a bit of fun with the project as well, so time slipped abit. (+ plus alittle extra to add some caching)

### what was missed

- The DB connection. I may come back later and go ahead with an implementation of the DB with Typeorm and postgresql.
  In order to accomadate that missing features, I used `localstorage` to hold information. Not a "great" alternative,
  but was good enough to show off the saving features.
- API "favoriting" calls. Ran out of time, but supplied local caching to show "how it would work".

### things I'd like to add

- Jest testing. Feels like it could help with code coverage stats and some usability.
- Login features. Would be amazing to be able to authenticate through Github and automatically store information.
- Separate "view" dashboards. Could be sweet to have a nav that can easily take me to different information.

### using application

#### running

1. Download the application
2. In a CLI within the project, run `npm install` or `yarn install`
3. Run `npm run build` or `yarn run build`
4. Run `npm run start` or `yarn run start` to start the application in port `3000`

#### using

It will show the most recent public gists.

- To search by username, you can type `username:{query}`
- To search by gist id, you can type `id:{query}`
- Regular searches will search by username

## quick stack stats

- React JS (NextJS)
- SWR
- Tailwind
- "Lambdas" (NextJS)
- typescript
- lodash
- typeorm + postgresql
- fontawesome
